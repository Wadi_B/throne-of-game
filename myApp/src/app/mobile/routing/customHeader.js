import React from "react"
// import { Header } from "react-navigation";

import { TouchableOpacity, Image, StyleSheet, View} from "react-native"
import { image } from "../../assets/image";

import {width, Normalize} from "../../commons/method/index"

const HeaderLeft = ({navigation}) => (
    <TouchableOpacity onPress={() => navigation.goBack()} 
    style={{bottom: (width("8%")), paddingHorizontal: width("5%")}}>
        <Image resizeMode={"contain"} 
        style={{width: Normalize(18), height: Normalize(18)}} 
        source={image.LeftArrow}/>
    </TouchableOpacity>
)

const HeaderRight = () => (
    <TouchableOpacity style={{bottom: (width("8%")), paddingHorizontal: width("5%")}}>
        <View>
            <View style={styles.point}/>
            <View style={styles.point}/>
            <View style={styles.point}/>
        </View>
    </TouchableOpacity>
)




const styles = StyleSheet.create({
    point: {
        width: 4, 
        height: 4, 
        borderRadius: 4, 
        marginBottom: 2.5, 
        borderWidth: 1, 
        borderColor: "white"
    }
})

export { HeaderLeft, HeaderRight }