import React from "react"
import {View} from "react-native"
import {createMaterialTopTabNavigator} from "react-navigation"
import { Color } from "../../assets/color/color";
import { Normalize, width } from "../../commons/method";

const screen1 = () => (<View style={{flex: 1, backgroundColor:Color.backgroundColor}}></View>)
const screen2 = () => (<View style={{flex: 1, backgroundColor:Color.backgroundColor}}></View>)
const screen3 = () => (<View style={{flex: 1, backgroundColor:Color.backgroundColor}}></View>)
const screen4 = () => (<View style={{flex: 1, backgroundColor:Color.backgroundColor}}></View>)
const TabNavigator = createMaterialTopTabNavigator({
    Stats: {
        screen: screen1
    },
    Duo: {
        screen: screen2
    },
    Matchs: {
        screen: screen3
    },
    Palmares: {
        screen: screen4,
    }
},{
    tabBarOptions: {
        activeTintColor: "white",
        inactiveTintColor: Color.subText,
        labelStyle: {
            fontSize: Normalize(8.5),
            fontFamily: 'Quicksand',
            fontWeight:'bold'
        },
        style: {
            backgroundColor: Color.BlackGrey,
            elevation: 0,
            shadowOpacity: 0,
            borderBottomWidth: 0,
            height: width("15%"),
            shadowColor: 'transparent',
            justifyContent:"center",
        },
        indicatorStyle: {
            backgroundColor: Color.pink,
        },
    }
})

export {TabNavigator}