import React from "react"
import {createAppContainer, createStackNavigator} from 'react-navigation'

import { Profil } from "../../commons/views/index";
import { Color } from "../../assets/color/color";

import { HeaderLeft, HeaderRight } from "./customHeader"

import {width} from "../../commons/method/index"

import { TabNavigator } from "./TopNavigator"

const Application = createStackNavigator({
    Profil: {
        screen: Profil,
        navigationOptions: ({navigation}) => ({
            headerRight: <HeaderRight navigation={navigation}/>,
            title: "PROFIL",
            headerStyle: {
                elevation: 0,
                shadowOpacity: 0,
                borderBottomWidth: 0,
                height: width("8%"),
                backgroundColor:Color.backgroundColor,
            },
            headerTitleStyle: {
                fontWeight:"400",
                fontFamily: 'Quicksand',
                bottom: width("8%"),
                color:"white"
            },
        }),
    }, 
    statsNavigator: {
        screen: TabNavigator,
        navigationOptions: ({navigation}) => ({
            title: "ASTRALIS TEAM",
            headerLeft: <HeaderLeft navigation={navigation}/>,
            headerRight: <HeaderRight navigation={navigation}/>,
            headerStyle: {
                elevation: 0,
                shadowOpacity: 0,
                borderBottomWidth: 0,
                height: width("8%"),
                backgroundColor:Color.backgroundColor,
            },
            headerTitleStyle: {
                fontWeight:"400",
                fontFamily: 'Quicksand',
                bottom: width("8%"),
                color:"white"
            },
        }),
    }
}, { initialRouteName: "Profil"})

// const Connection = createStackNavigator({

// })

// const ConnectionContainer = createAppContainer(Connection)
const ApplicationContainer = createAppContainer(Application)

export {ApplicationContainer}