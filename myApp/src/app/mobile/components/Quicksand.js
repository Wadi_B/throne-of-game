import React from "react"
import {Text, StyleSheet} from "react-native"

const Quicksand = ({children, style}) => (
    <Text style={[styles.quicksand, style]}>{children}</Text>
)

const styles = StyleSheet.create({
    quicksand: {
        fontFamily: "Quicksand"
    }
})
export {Quicksand}