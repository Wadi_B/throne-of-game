import React from 'react'
import {View, StyleSheet} from "react-native"
import { Color } from '../../assets/color/color';

const BackgroundContainer = (props) => (
    <View {...props} style={styles.background}></View>
)

const styles = StyleSheet.create({
    background: {
        backgroundColor: Color.backgroundColor,
        flex: 1
    }
})

export {BackgroundContainer}