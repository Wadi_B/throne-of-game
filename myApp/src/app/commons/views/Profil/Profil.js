import React, {Component} from "react"
import {StyleSheet, TouchableOpacity, View, Image} from "react-native"

import {Quicksand} from "../../../mobile/components/Quicksand"
import {BackgroundContainer} from "../../components/backgroundContainer"
import { width, Normalize } from "../../method";
import { image } from "../../../assets/image";
import { Color } from "../../../assets/color/color";

export default class Profil extends Component {
    constructor(props) {
        super(props)
        this.state = {}
    }

    render() {
        const {navigate} = this.props.navigation
        return(
            <BackgroundContainer style={{justifyContent: 'center', alignItems: 'center',}}>
                <View style={styles.subContainer}>
                    <View style={styles.imageContainer}>
                        <Image resizeMode={"contain"} style={{width: "100%", height:"100%"}} source={image.jon}/>
                    </View>
                    <Quicksand style={styles.text}>Jonathan Meslien</Quicksand>
                    <TouchableOpacity  style={styles.buttonStat} onPress={() => navigate("statsNavigator")}>
                        <Quicksand style={{color: Color.pink}}>Statistique</Quicksand>
                    </TouchableOpacity>
                </View>
            </BackgroundContainer>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    imageContainer: {
        width: width("27%"), 
        height: width("27%"), 
        borderRadius: width("27%"), 
        backgroundColor:"rgba(0,0,0,0.1)" ,
        borderColor: "white",
        overflow: 'hidden',
        borderWidth: 1,
    },
    buttonStat: {
        width: width("32%"), 
        borderWidth: 1, 
        borderColor: Color.pink, 
        height: width("9%"), 
        borderRadius: width("2%"), 
        justifyContent: 'center', 
        alignItems: "center"
    },
    text: {
        color:"white", 
        fontWeight:"bold", 
        fontSize: Normalize(13), 
        marginVertical: width("3%")
    },
    subContainer: {
        width: "100%", 
        height: width("60%"), 
        backgroundColor:"rgba(0,0,0,0.1)" ,
        justifyContent:"flex-end", 
        alignItems:"center"
    }
})