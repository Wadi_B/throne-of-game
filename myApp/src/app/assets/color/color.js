import React from "react"


const Color = {
    backgroundColor: "#18171D", 
    BlackGrey: "#252430", 
    pink: "#FC7D87", 
    white: "white",
    subText: "#A5A4B7"
}

export {Color}