import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Platform,
} from 'react-native';

import { Color } from './app/assets/color/color';

class App extends Component {
  render() {
    return (
        <View style={styles.container}>
          <View style={styles.subContainer}>
          </View>
        </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    height: "100vh"
  },
  subContainer: {
    flex: 1,
    backgroundColor: Color.backgroundColor
  }
});

let hotWrapper = () => () => App;
if (Platform.OS === 'web') {
  const { hot } = require('react-hot-loader');
  hotWrapper = hot;
}
export default hotWrapper(module)(App);
