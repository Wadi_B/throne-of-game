import React, { Component } from 'react';
import {
  StyleSheet,
  StatusBar,
  SafeAreaView,
} from 'react-native';

import {ApplicationContainer} from "./app/mobile/routing/index"

import { Color } from './app/assets/color/color';


class AppMobile extends Component {
  render() {
    return (
        <SafeAreaView style={styles.container}>
            <StatusBar barStyle="light-content"/>
            <ApplicationContainer />
        </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor:Color.backgroundColor
  },
  subContainer: {flex: 1, backgroundColor: Color.backgroundColor}
});

export default AppMobile;
